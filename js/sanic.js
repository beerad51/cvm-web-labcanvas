function Sanic(){
	this.x = 50;
	this.size = 10;
	this.baseY =  riverY-this.size/2-14;
	this.y = 0;
	this.speedX = 0;
	this.speedY = 0;
	this.baseAccel = 0.2;
	this.accel = this.baseAccel;
	this.topSpeed = 20;
	this.vertPower = 10;
	this.alive = true;
	this.flipped = new TiledImage("images/sonicFlipped.png", 10, 3);
	this.notFlipped = new TiledImage("images/sonic.png", 10, 3);
	this.sprite = this.notFlipped;
	this.sprite.changeRow(0);
	this.sprite.changeColumnInterval(0,0);
	this.height = 30;
}

Sanic.prototype.tick = function () {
	
	if (this.alive){
		this.sprite.tick(ctx, this.x, this.y-this.height);
		this.move();
		return true;
	}
	return false;
	
}
Sanic.prototype.move = function () {
	//detect if dead
	if (this.y > this.baseY+20){
		this.alive = false;
		newGame=true;
		spacePushed = false;
		score = 0;
	}
	//top speed
	if (Math.abs(this.speedX)>this.topSpeed){
		if (this.speedX>0){
			this.speedX=this.topSpeed;
		}
		else{
			this.speedX = -this.topSpeed;
		}
	}
	//decrease speed if on ground and no button pressed
	if (!rightPushed && !leftPushed && (this.y == this.baseY)){
		if (Math.abs(this.speedX)<0.1){
			this.speedX=0;
		}
		if (this.speedX>0){
			this.speedX -= this.baseAccel/3;
		}
		else if (this.speedX<0){
			this.speedX += this.baseAccel/3;
		}
	}
	//move horizontally
	if (rightPushed){
		this.sprite=this.notFlipped;
		this.sprite.changeRow(0);
		this.sprite.changeColumnInterval(2,10);
		this.height = 30;
		this.speedX +=this.accel;
	}
	else if (leftPushed){
		this.sprite=this.flipped;
		this.sprite.changeRow(0);
		this.sprite.changeColumnInterval(2,10);
		this.height = 30;
		this.speedX -=this.accel;
	}
	//move x
	this.x+=this.speedX;
	//décaler les blocs
	if (this.x>=500){
		pushBackBlocks(this.x-500);
		this.x=500;
	}
	//apply gravity
	this.speedY+=gravity
	//detect terrain
	if (this.y>=this.baseY){
		if (this.detectTerrain()){
			this.speedY =0;
			this.y=this.baseY;
		}
	}
	//jump
	if (this.y==this.baseY){
		if (spacePushed){
			this.speedY = -this.vertPower;
		}
	}
	//move y
	this.y+=this.speedY;

	//turbo boost
	if (downPushed){
		this.sprite.changeRow(2);
		this.sprite.changeColumnInterval(1,8);
		this.height = 0;
		if (this.y == this.baseY ){
			this.accel += 0.05;
		}
		
	}
	// sprite not moving if speed == 0
	else if (this.speedX==0){
		this.sprite.changeRow(0);
		this.sprite.changeColumnInterval(0,0);
		this.height = 30;
	}
	//reset accel
	else{
		this.accel = this.baseAccel;
	}
}


Sanic.prototype.detectTerrain = function (){
	for (var i = 0; i < blockList.length; i++) {
		if (this.x >= blockList[i].x -10 
			&& this.x <= blockList[i].x+blockList[i].width + 10){
			return true;
	}
}
return false;
}