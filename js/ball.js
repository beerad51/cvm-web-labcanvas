function Ball(){
	this.x = 50;
	this.size = 10;
	this.baseY =  riverY-this.size/2-5;
	this.y = 0;
	this.speedX = 0;
	this.speedY = 0;
	this.accel = 0.3;
	this.topSpeed = 5;
	this.jumpHeight = 150;
	this.vertPower = 10;
	this.grounded = true;
	this.alive = true;
}

Ball.prototype.tick = function () {
	
	if (this.alive){
		ctx.beginPath();
		ctx.arc(this.x,this.y, this.size, 0, 2 * Math.PI, false);
		ctx.fillStyle = "green";
		ctx.fill();
		ctx.strokeStyle = "red";
		ctx.stroke();
		this.move();
		return true;
	}
	return false;
	
}
Ball.prototype.move = function () {

	if (this.y>700){
		this.alive=false;
	}
	// top speed
	if (Math.abs(this.speedX)>this.topSpeed){
		if (this.speedX>0){
			this.speedX = this.topSpeed;
		}
		else{
			this.speedX = -this.topSpeed;
		}
	}
	// decrease speed if on ground and no button is pressed
	if (!rightPushed && !leftPushed && (this.y == this.baseY)){
		if (Math.abs(this.speedX)<0.1){
			this.speedX = 0;
		}
		if (this.speedX>0){
			this.speedX -= this.accel/3;
		}
		else if (this.speedX<0){
			this.speedX += this.accel/3;
		}
	}
	// move horizontally
	if (rightPushed){
		this.speedX +=this.accel;
	}
	else if (leftPushed){
		this.speedX -= this.accel;
	}
	//décaler les blocs 
	this.x+=this.speedX;
	if (this.x>=500){
		pushBackBlocks(this.x-500);
		this.x=500;
	}
	this.speedY+=gravity;
	// bounce
	if (this.y>=this.baseY){
		if (this.detectTerrain()){
			this.speedY = -this.speedY/1.5;
			this.grounded = true;
			if (Math.abs(this.speedY)<0.5){
				this.speedY=0;
		}
	}

}
if (spacePushed){

	if (this.y < this.baseY){
		;
	}

	else if (this.grounded){
		this.speedY = -this.vertPower;
		this.grounded = false;
	}
	else{
		if (this.speedY>=0){
			this.speedY =0;
		} 
		this.speedY+=gravity;
	} 
}
this.y+=this.speedY;
}

Ball.prototype.detectTerrain = function (){
	for (var i = 0; i < blockList.length; i++) {
		if (this.x >= blockList[i].x 
			&& this.x <= blockList[i].x+blockList[i].width){
			return true;
	}
}
return false;
}