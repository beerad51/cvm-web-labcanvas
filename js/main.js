var ctx = null;
var ball = null;
var riverY = 660;
var rightPushed = false;
var leftPushed = false;
var spacePushed = false;
var downPushed = false;
var gravity = 0.3;
var blockList = [];
var difficulty = 0.9; 
var blockSize = 40;
var newGame = true;
var firstTime = true;
var score=0;
var highScore = 0;

function pushBackBlocks (distance){
	for (var i = 0; i < blockList.length; i++) {
		blockList[i].x-=distance;
		// blockList[i].tick();
	}
}

window.onload = function () {

	ctx = document.getElementById("canvas").getContext("2d");
	tick();
	
	function tick() {
		ctx.clearRect(0,0, 1000,800);
		drawBg();
		if (score>=highScore){
			highScore = score;
		}
		if (newGame){
			score=0;
			if (firstTime || gameOver()){
				firstTime = false;
				ball = new Sanic();
				for (i=0;i<10;i++){
					blockList.push(new Block(i*blockSize));
				}
				newGame = false;
			}
		}
		else{
			drawUI();
		}
		detectMovement();
		if (ball){
			ball.tick();
		}
		for (var i = 0; i < blockList.length; i++) {
			var current = blockList[i];
			var alive = current.tick();
			if (!alive){
				blockList.splice(i,1);
				i--;
				score+=1;
			}
		}
		if (blockList.length<20){
			createBlocks();
		}
		window.requestAnimationFrame(tick);
	}

	function createBlocks (){
		for (var i = 0 ; i < 150; i++) {
			if (Math.random() > difficulty) {
				blockList.push(new Block( i * blockSize+1000));
			}
		}

	}
	function drawBg() {
		ctx.clearRect(0,0,1000,700);
		// river
		ctx.fillStyle = "lightskyblue";
		ctx.fillRect(0,riverY,1000,110);
	};
	function drawUI() {
		ctx.font = "30px bit"
		ctx.fillStyle = "blue";
		ctx.fillText("Score", 15, 50);
		ctx.fillText( score, 200, 50);

		ctx.fillText("High Score", 550, 50);
		ctx.fillText(highScore, 850, 50);

	}
	function gameOver(){
		ctx.font = "90px bit";
		ctx.fillStyle = "red";
		ctx.fillText("GAME OVER", 125 ,250);
		ctx.font = "30px bit";
		ctx.fillText("Press spacebar to try again", 150, 350);
		if (spacePushed){
			return true;
		}
		else{
			return false;
		}
	}
	function detectMovement (){

		document.addEventListener("keydown", function (e){
			if (e.which==65){
				leftPushed = true;
			}
			else if (e.which==68){
				rightPushed = true;
			}
			if (e.which==32){
				spacePushed = true;
			}
			if (e.which==83){
				downPushed = true;
			}
			
		});
		document.addEventListener("keyup", function (e){
			if (e.which==65){
				leftPushed = false;
			}
			else if (e.which==68){
				rightPushed = false;
			}
			if (e.which==32){
				spacePushed = false;
			}
			if (e.which==83){
				downPushed = false;
			}
		});
	};
	
};